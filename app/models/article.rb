class Article < ActiveRecord::Base
  has_many :comments
  has_many :taggings
  has_many :tags, through: :taggings
  
  def tag_list
    self.tags.collect do |tag|
      tag.name
    end.join(", ")
  end
  
  def tag_list=(tags_string)
    tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq
    new_or_found_tags = []
    tag_names.each do |tag_name|
      if Tag.find_by_name(tag_name)
        new_or_found_tags << Tag.find_by_name(tag_name)
      else
        Tag.create(name: tag_name)
      end
    end
    
    self.tags = new_or_found_tags
  end
end
